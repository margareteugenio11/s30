const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));



mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.tmxqb.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;


    db.on("error", console.error.bind(console, "Connection Error"))


    db.once('open', () => console.log('Connected to the cloud database'))

const userSchema = new mongoose.Schema({

	username: String,
	password: String

})

const User = mongoose.model('User', userSchema)

app.post('/signup', (req,res) =>{

	User.findOne({username: req.body.username}, (err, result)=>{

		if(result != null && result.username === req.body.username){

				return res.send('Duplicate error. Change username.')
		} else {

			let newUser = new User({

				username: req.body.username
			})

			newUser.save((saveErr, saveUser) => {

				if(saveErr){

					return console.error(saveUser)
				} else {

					return res.status(201).send('created new username')
				}
			})
		}

	})

})

app.get('/signup', (req,res) => {

	User.find({}, (err,result)=>{

		if(err){

			return console.log(err);
		}else {

			return res.status(200).json({
				data:result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))